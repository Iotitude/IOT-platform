import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.io.FileReader;
import java.util.ArrayList;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.io.File;
import java.text.SimpleDateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.kaaproject.kaa.client.DesktopKaaPlatformContext;
import org.kaaproject.kaa.client.Kaa;
import org.kaaproject.kaa.client.KaaClient;
import org.kaaproject.kaa.client.SimpleKaaClientStateListener;
import org.kaaproject.kaa.client.configuration.base.ConfigurationListener;
import org.kaaproject.kaa.client.configuration.base.SimpleConfigurationStorage;
import org.kaaproject.kaa.client.logging.strategies.RecordCountLogUploadStrategy;

import iotitude.sensehat.config.SenseHatConf;
import iotitude.sensehat.log.SenseLog;

public class SenseHat {

    private static final Logger LOG = LoggerFactory.getLogger(SenseHat.class);

    private static KaaClient kaaClient;

    private static ScheduledFuture<?> scheduledFuture;
    private static ScheduledExecutorService scheduledExecutorService;
    static JSONParser parser = new JSONParser();
    
    public static void main(String args[]) {
      LOG.info("HELLO");
      
      scheduledExecutorService = Executors.newScheduledThreadPool(1);

      DesktopKaaPlatformContext desktopKaaPlatformContext = new DesktopKaaPlatformContext();
      kaaClient = Kaa.newClient(desktopKaaPlatformContext, new FirstKaaClientStateListener(), true);

      RecordCountLogUploadStrategy strategy = new RecordCountLogUploadStrategy(1);
      strategy.setMaxParallelUploads(1);
      kaaClient.setLogUploadStrategy(strategy);

      kaaClient.setConfigurationStorage(new SimpleConfigurationStorage(desktopKaaPlatformContext, "saved_config.cfg"));

      kaaClient.addConfigurationListener(new ConfigurationListener() {
          @Override
          public void onConfigurationUpdate(SenseHatConf configuration) {
              LOG.info("Received configuration data. New sample period: {}", configuration.getInteval());
              // onChangedConfiguration(TimeUnit.SECONDS.toMillis(configuration.getSamplePeriod()));
          }
      });

      kaaClient.start();
      int i=1;
      kaaClient.start();
      LOG.info("--= Press any key to exit =--");
      while(i==1);
      LOG.info("Stopping...");
      scheduledExecutorService.shutdown();
      kaaClient.stop();
    }

	private static ArrayList<String> getJsonData() {
    	
    	 Object obj;
    	 ArrayList<String> ar = new ArrayList<String>();
    	 ArrayList<String> error = new ArrayList<String>();
		 try {					 
		 obj = parser.parse(new FileReader("/home/pi/data/data.json"));	
		 JSONObject jsonObject =  (JSONObject) obj;	
		 String ar1 = jsonObject.get("humidity").toString();
	         String ar2 = jsonObject.get("temperature").toString();
	         String ar3 = jsonObject.get("pressure").toString();
	         ar.add(ar1);
	         ar.add(ar2);
	         ar.add(ar3);
	         return ar;
	            
		} catch (IOException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			LOG.error("PARSINTA" + e.toString());
		}
		return error;

    }

    private static void onKaaStarted(long time) {
        if (time <= 0) {
            LOG.error("Wrong time is used. Please, check your configuration!");
            kaaClient.stop();
            System.exit(0);
        }
        scheduledFuture = scheduledExecutorService.scheduleAtFixedRate(
                new Runnable() {
                    @Override
                    public void run() {                   	
                        ArrayList<String> List = getJsonData();
                        kaaClient.addLogRecord(new SenseLog(List.get(0), List.get(1), List.get(2)));                       
                        LOG.info("Data from json: "+ List.toString());
                    }
                }, 0, time, TimeUnit.MILLISECONDS);
    }

    private static class FirstKaaClientStateListener extends SimpleKaaClientStateListener {

        @Override
        public void onStarted() {
            super.onStarted();
            LOG.info("Kaa client started");
            SenseHatConf configuration = kaaClient.getConfiguration();
            LOG.info("Default interval: {}", configuration.getInteval());
            onKaaStarted(TimeUnit.SECONDS.toMillis(configuration.getInteval()));
        }

        @Override
        public void onStopped() {
            super.onStopped();
            LOG.info("Kaa client stopped");
        }
    }
}
